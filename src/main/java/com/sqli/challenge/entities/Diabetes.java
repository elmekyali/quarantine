package com.sqli.challenge.entities;

public class Diabetes implements State {
    @Override
    public String draw() {
        return "D";
    }

    @Override
    public void updateState(Patient patient) {
        if (!patient.isUsingInsulin() && patient.isWaiting40Days())
            patient.setState(new Dead());

        if (patient.isUsingParacetamol() && patient.isUsingAspirin())
            patient.setState(new Dead());
    }
}

package com.sqli.challenge.entities;

public class Healthy implements State {
    @Override
    public String draw() {
        return "H";
    }

    @Override
    public void updateState(Patient patient) {
        if (patient.isUsingInsulin() && patient.isUsingAntibiotic())
            patient.setState(new Fever());

        if (patient.isUsingParacetamol() && patient.isUsingAspirin())
            patient.setState(new Dead());
    }
}

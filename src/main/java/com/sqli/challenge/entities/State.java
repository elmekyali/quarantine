package com.sqli.challenge.entities;

public interface State {
    String draw();

    void updateState(Patient patient);
}

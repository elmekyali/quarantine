package com.sqli.challenge.entities;

public class Patient {
    private State state;
    private boolean isUsingAspirin;
    private boolean isUsingAntibiotic;
    private boolean isUsingInsulin;
    private boolean isWaiting40Days;
    private boolean isUsingParacetamol;

    public Patient(String patientState) {
        isUsingAspirin = false;
        isUsingAntibiotic = false;
        isUsingInsulin = false;
        isWaiting40Days = false;
        isUsingParacetamol = false;

        switch (patientState) {
            case "H" :
                this.state = new Healthy(); break;
            case "D":
                this.state = new Diabetes(); break;
            case "F":
                this.state = new Fever(); break;
            case "T":
                this.state = new Tuberculosis(); break;
        }
    }

    public String report() {
        return this.state.draw();
    }


    void setState(State state) {
        this.state = state;
    }

    public void wait40Days() {
        isWaiting40Days = true;
        state.updateState(this);
    }

    Boolean isUsingAspirin() {
        return isUsingAspirin;
    }

    public void aspirin() {
        isUsingAspirin = true;
        state.updateState(this);
    }

    boolean isUsingAntibiotic() {
        return isUsingAntibiotic;
    }

    public void antibiotic() {
        isUsingAntibiotic = true;
        state.updateState(this);
    }

    boolean isUsingInsulin() {
        return isUsingInsulin;
    }

    public void paracetamol() {
        isUsingParacetamol = true;
        state.updateState(this);
    }

    public void insulin() {
        isUsingInsulin = true;
        state.updateState(this);
    }

    boolean isWaiting40Days() {
        return isWaiting40Days;
    }

    void setUsingAntibiotic(boolean usingAntibiotic) {
        isUsingAntibiotic = usingAntibiotic;
    }

    public boolean isUsingParacetamol() {
        return isUsingParacetamol;
    }
}

package com.sqli.challenge.entities;

public class Dead implements State {
    @Override
    public String draw() {
        return "X";
    }

    @Override
    public void updateState(Patient patient) {

    }
}

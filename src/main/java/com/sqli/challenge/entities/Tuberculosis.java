package com.sqli.challenge.entities;

public class Tuberculosis implements State {
    @Override
    public String draw() {
        return "T";
    }

    @Override
    public void updateState(Patient patient) {
        if (patient.isUsingAntibiotic()) {
            patient.setState(new Healthy());
            patient.setUsingAntibiotic(false);
        }
        if (patient.isUsingParacetamol() && patient.isUsingAspirin())
            patient.setState(new Dead());
    }
}

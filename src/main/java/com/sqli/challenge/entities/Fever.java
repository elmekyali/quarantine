package com.sqli.challenge.entities;

public class Fever implements State {
    @Override
    public String draw() {
        return "F";
    }

    @Override
    public void updateState(Patient patient) {
        if (patient.isUsingAspirin() || patient.isUsingParacetamol())
            patient.setState(new Healthy());

        if (patient.isUsingParacetamol() && patient.isUsingAspirin())
            patient.setState(new Dead());
    }
}

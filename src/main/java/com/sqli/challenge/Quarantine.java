package com.sqli.challenge;

import com.sqli.challenge.entities.Patient;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Quarantine {
    private final List<Patient> patients;
    private Map<String, Long> patientsReport = new LinkedHashMap<String, Long>(){{
        put("F",0L);
        put("H",0L);
        put("D",0L);
        put("T",0L);
        put("X",0L);
        }
    };

    public Quarantine(String patientList) {
        this.patients = Arrays.stream(patientList.split(",")).map(Patient::new).collect(Collectors.toList());
    }

    public String report() {
        patients.stream()
                .collect(Collectors.groupingBy(Patient::report, Collectors.counting()))
                .forEach((key, val) -> patientsReport.merge(key, val, (oldValue, newValue) -> oldValue + newValue));

        return patientsReport.entrySet()
                .stream()
                .map(entryValue -> String.format("%s:%s", entryValue.getKey(), entryValue.getValue()))
                .collect(Collectors.joining(" "));
    }

    public void wait40Days() {
        this.patients.forEach(Patient::wait40Days);
    }

    public void aspirin() {
        this.patients.forEach(Patient::aspirin);
    }

    public void antibiotic() {
        this.patients.forEach(Patient::antibiotic);
    }

    public void insulin() {
        this.patients.forEach(Patient::insulin);
    }

    public void paracetamol() {
        this.patients.forEach(Patient::paracetamol);
    }
}
